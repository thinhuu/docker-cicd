import React from 'react'
import { Navigate } from 'react-router';

type Props = {
    Component:React.ReactNode,
    role:string
}

const Protected = ({Component,role}:Props) => {
    const token = false;
    const roles = ['USER,ADMIN']
  return (
    <div>
      {
        token ? Component : <Navigate to='/login'/>
      }
    </div>
  )
}

export default Protected