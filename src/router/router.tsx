import { createBrowserRouter } from "react-router-dom"
import { lazy } from 'react';

import Protected from "./protected";


const Login = lazy(() => import("../pages/auth/login/login"));
const LayoutUser = lazy(() => import("../layout/layoutUser"));
const Report = lazy(() => import("../pages/user/report/report"));
const Ticket = lazy(() => import("../pages/user/ticket/ticket"));
const LayoutAdmin = lazy(() => import("../layout/layoutAdmin"));
const Dashboard = lazy(() => import("../pages/admin/dashboard/dashboard"));
const Student = lazy(() => import("../pages/admin/student/student"));
const NotFound = lazy(() => import("../components/error/notFound"));
const LayoutRoot = lazy(() => import("../layout/layoutRoot"));



export const router = createBrowserRouter([
  {
    path:'/',
    index:true,
    element: <LayoutRoot />,
  },
  {
    path: "/login",
    element: <Login />,
    index: true,
  },
  {
    element: <Protected Component={<LayoutUser />} role='USER' />,
    children: [{
      path: 'report',
      element: <Report />
    },
    {
      path: 'ticket',
      element: <Ticket />
    }]
  }, 
  {
    element: <Protected Component={<LayoutAdmin />} role='ADMIN' />,
    children: [{
      path: 'dashboard',
      element: <Dashboard />
    },
    {
      path: 'student',
      element: <Student />
    }]
  },
  {
    path: '*',
    element: <NotFound />
  }
])

