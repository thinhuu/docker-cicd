import { Outlet } from "react-router";

const LayoutAdmin = () => {
    return (
        <>
        <Outlet/>
        </>
    )
}
export default LayoutAdmin