import react, { useEffect } from 'react'
import { Outlet, useNavigate } from 'react-router'


const LayoutRoot = () => {
    const navigate = useNavigate();
    const role = ['USER','ADMIN']
     useEffect(() => {
      if(role.includes('USER')){
       return navigate('/report')
      }
      if(role.includes('ADMIN')){
        return navigate('/dashboard')
      }
      return navigate('/login')
     },[])
    return (
        <>
            <Outlet />
        </>
    )
}
export default LayoutRoot